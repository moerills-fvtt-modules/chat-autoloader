Hooks.on('renderChatLog', (app, html, data) => {
    const render_limit = 0.2;
    let log = html.find("#chat-log");

    // Function to render new messages until limit is reached
    // we want rendering to happen asynchronous (responsiveness also to await message.render)
    // =>outsource it into async function
    let renderMessages =  async function(app, lastEl) {
        // Define a lock variable, due to scroll events possibly coming faster than rendering
        // The critical section will still accommodate for that, since it updates the ending condition
        // in each iteration
        // no atomic needed, due to javascript being single threaded. \o/
        app._renderingMoreMessages = true; 
        for (let i = lastEl - 1; i >= 0; i--) {
            const scrollPos = log.scrollTop();
            const message = game.messages.entities[i];
            if (!message.visible) continue;
            const html = await message.render();
            log.prepend(html);
            app._lastRenderedEl = i;
            log.scrollTop(scrollPos + html.outerHeight())
            const scrollPerc = log.scrollTop() / log[0].scrollHeight;
            if (scrollPerc > render_limit)
                break;
        }
        app._renderingMoreMessages = false;
    }
    
    // on scroll get Scrollbar position. If position is high enough,load more messages
    log.on('scroll', ev => {
        if (app._lastRenderedEl === 0 || app._renderingMoreMessages)
            return;
        const scrollPerc = log.scrollTop() / log[0].scrollHeight;

        // Start rerendering when above top 1%
        if (scrollPerc < 0.01)
            renderMessages(app, app._lastRenderedEl);
    })
});

// overwrite render method to only render messages until 2x height is reached
ChatLog.prototype._render = async function(...args) {
    await SidebarTab.prototype._render.call(this,  ...args);
    let log = this.element.find("#chat-log");
    log.html("");   // Set log to nothing
    const height = log[0].scrollHeight;
    await getTemplate("templates/sidebar/chat-message.html");
    // Backwards traversal, since we want to render only the newest messages
    let count = 0;
    for (let i = game.messages.entities.length - 1; i >= 0 ; i--) {
        const message = game.messages.entities[i];
        if (!message.visible) continue;

        const html = await message.render();
        log.prepend(html);  // Prepend instead of append, due to traversing backwards
        
        this._lastRenderedEl = i;
        if (log[0].scrollHeight > 5*height)
            break;
        count++
    }
    this.scrollBottom();
    this._setup = 1;
}

const prev_deleteAll = ChatLog.prototype.deleteAll;
ChatLog.prototype.deleteAll = function() {
    this._lastRenderedEl = 0;
    prev_deleteAll();
}