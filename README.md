
# chat Autoloader  
![FVTT Version](https://img.shields.io/badge/FVTT-%3E%3D%200.4.1-critical)  
[![MIT-License](https://img.shields.io/badge/License-MIT-black?style=flat-square)](https://gitlab.com/moerills-fvtt-modules/chat-autoloader/raw/master/LICENSE) 
[![PayPal](https://img.shields.io/badge/Buy%20Me%20A%20Coffee-PayPal-blue?style=flat-square)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url)  

This module renders only a few chat messages at start-up to improve loading times.
Older messages will be loaded automatically on demand, when scrolling to the top.  
This only affects messages loaded on page load. Newly created messages will be displayed as expected and older messages won't be removed.

## Installation
* Go to the *Add-On Modules* tab in FVTTs setup screen
* Press *Install Module*
* Paste ``https://gitlab.com/moerills-fvtt-modules/chat-autoloader/raw/master/module.json`` into the text field
* Press *Install*

# License
This work is licensed under the MIT License and Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).
	