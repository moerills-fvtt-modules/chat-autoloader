const fs = require('fs');
const path = require('path');
const util = require('./util');
const moduleJson = util.readModuleJson();

let v = moduleJson.version.split(".");
if (v.length < 2)
	v.push('1');
else
	v[1] = Number(v[1])+1;
if (v.length > 2)
	v = v.slice(0, 2);
moduleJson.version = v.join(".");

fs.writeFileSync(path.join(util.getBaseDir(), "module.json"), JSON.stringify(moduleJson, null, 2));

util.setChangelogVersion(`Update v${moduleJson.version}`)